# Hard Color Map Converter

## What is this tool for?

HCMConvert is a tool that converts pictures to the Hard Color Map (HCM) format intruduced by the REHARDEN demo for Atari 8-bit computers (64K PAL XL/XE).

A few no-flicker/non-interlace examples:

![Tiger](readme_pics/tiger.png)
![Darth Vader](readme_pics/darthvader.png)
![Parrots](readme_pics/parrot.png)
![King Tut](readme_pics/tut1.png)
![King Tut](readme_pics/tut2.png)
![Orca](readme_pics/orca.png)


## What does this repo include?

This repo includes the full source code of HCMConvert in the form of an Eclipse Neon project.

There's no separate documentation written for this tool, please see the source code for examples on usage (main function).

Please click the link below for details on the HARD Color Map graphics format:

- [Hard Color Map a.k.a. HCM documentation](https://bitbucket.org/sandor-hard/reharden/src/master/HCM.md)
- [HCM Viewer Example code for Atari XL/XE](https://bitbucket.org/sandor-hard/hcm-viewer-example/)

Sandor Teli / HARD
