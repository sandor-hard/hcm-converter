package com.hardsoft.hcm.convert;

import com.hardsoft.hcm.convert.plugins.HcmPlugin;
import com.hardsoft.hcm.convert.plugins.HcmPluginModeF;

public class HcmConversionProfileModeFMixGrey extends HcmConversionProfile {

	public HcmConversionProfileModeFMixGrey() {

		IGNORE_DITHERING_BELOW_BRIGHTNESS = 0.0f; //Set to 0.5f for Pseudo b&w interlaced; or 0.25 for colorful non-interlaced
		IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE = 0.000001f;
		RESPECT_BRIGHTNESS_ONLY_RATIO = 1f; //1f = Pseudo-black&white; 0.99f-0.95f = colors+brightness
		GAMMA = 1f; //1f==original image
		DITHER_MATRIX_DIMENSION_POW = 1;
		MAX_FROM_TO_DISTANCE_TO_DITHER = 0.5f;
		UNDERLAY_DITHERING = true; //not tested since this setting was introduced

		MIX_PM_PF = true; //Does this make any difference here? (PRIOR)
		COLOR_BCK = 0x00; //outer border (doesn’t matter)
		COLOR_UL1 = 0x04;
		COLOR_UL2 = 0x0a;
		COLOR_PF0 = 0x00; //not used
		COLOR_PF1 = 0x08; //foreground color (only luminance mixes with UL/PF2)
		COLOR_PF2 = 0x00; //play field background
		
	}

	@Override
	HcmPlugin getPlugin() {
		return new HcmPluginModeF();
	}
	
}
