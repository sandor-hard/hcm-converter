package com.hardsoft.hcm.convert;

import com.hardsoft.hcm.convert.plugins.HcmPlugin;

abstract class HcmConversionProfile {

	double IGNORE_DITHERING_BELOW_BRIGHTNESS;
	double IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE;
	double RESPECT_BRIGHTNESS_ONLY_RATIO;
	double GAMMA;
	int DITHER_MATRIX_DIMENSION_POW;
	double MAX_FROM_TO_DISTANCE_TO_DITHER;
	boolean ALLOW_BLACK_DARKEST_DITHERING;
	boolean UNDERLAY_DITHERING;
	double MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY;	
	
	boolean MIX_PM_PF;
	int COLOR_BCK;
	int COLOR_UL1;
	int COLOR_UL2;
	int COLOR_PF0;
	int COLOR_PF1;
	int COLOR_PF2;
	
	abstract HcmPlugin getPlugin();
	
}
