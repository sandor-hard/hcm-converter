/*
    HARD Color Map converter plugin
    to be used with HCM Atari XL/XE as introduced in the REHARDEN demo

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to improve and/or add new HCM modes
    (like interlace modes; char based modes using the VSCROL trick; modes mixed with GTIA modes; etc...)
 */

package com.hardsoft.hcm.convert.plugins;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.marvinproject.image.color.skinColorDetection.ColorSpaceConverter;
import org.marvinproject.image.render.text.Text;

import com.hardsoft.hcm.convert.plugins.HcmPlugin.AtariColorSpec;

import marvin.gui.MarvinAttributesPanel;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.io.MarvinImageIO;
import marvin.plugin.MarvinAbstractImagePlugin;
import marvin.plugin.MarvinImagePlugin;
import marvin.util.MarvinAttributes;
import marvin.util.MarvinPluginLoader;

public abstract class HcmPlugin extends MarvinAbstractImagePlugin
{
	
	public static final int HCM_VERSION = 0x01;


    //Feel free to add new HCM modes
    //(like interlace modes; char based modes using the VSCROL trick; modes mixed with GTIA modes; etc...)

    //Not using bits here as we might want to support entirely custom modes with no rules/attributes matching the current ones
	//implemented via custom kernels for each new mode where necessary
	public static final int MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_9_COLOR = 0;
	public static final int MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_9_COLOR_INTERLACED = 1;
	public static final int MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_7_COLOR = 2;
	public static final int MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_7_COLOR_INTERLACED = 3;
	public static final int MODE_DESCRIPTOR_VALUE_ANTIC_F_28_BYTES_8_COLOR = 4;
	public static final int MODE_DESCRIPTOR_VALUE_ANTIC_F_28_BYTES_8_COLOR_INTERLACED = 5;
	
	final int COLOR_MARGIN_LEFT = 2*8;
	final int COLOR_MARGIN_RIGHT = COLOR_MARGIN_LEFT;
	
	final int WIDTH = 256;
	final int HEIGHT = 192;
	
//  ----- 16 bytes header+palette -----
	
//  ----- 7 byte header -----
	final int HCM_FINGERPRINT_DATA_START = 0;
	final String HCM_FINGERPRINT = "HCMA8";
	final int HCM_FINGERPRINT_DATA_SIZE = HCM_FINGERPRINT.length();
	final int VERSION_DESCRIPTOR = HCM_FINGERPRINT_DATA_START+HCM_FINGERPRINT_DATA_SIZE;
	final int MODE_DESCRIPTOR = VERSION_DESCRIPTOR+1;
	final int HEADER_DATA_LENGTH = HCM_FINGERPRINT_DATA_SIZE+2;
//  ----- 7 byte header -----
	
//  ----- 9 byte colors -----
	final int PALETTE_DATA_START = HEADER_DATA_LENGTH;
	final int PALETTE_DATA_BCK = PALETTE_DATA_START+0;
	final int PALETTE_DATA_UNDERLAY1 = PALETTE_DATA_START+1;
	final int PALETTE_DATA_UNDERLAY2 = PALETTE_DATA_START+2;
	final int PALETTE_DATA_PLAYFIELD0 = PALETTE_DATA_START+3;
	final int PALETTE_DATA_PLAYFIELD1 = PALETTE_DATA_START+4;
	final int PALETTE_DATA_PLAYFIELD2 = PALETTE_DATA_START+5;
	final int PALETTE_DATA_CMB1 = PALETTE_DATA_START+6;
	final int PALETTE_DATA_CMB2 = PALETTE_DATA_START+7;
	final int PALETTE_DATA_CMB3 = PALETTE_DATA_START+8;
	final int PALETTE_DATA_SIZE = 9;
//  ----- 9 byte colors -----
	
//  ----- end of 16 bytes header+palette -----

	final int SPRITE_DATA_START = PALETTE_DATA_START + PALETTE_DATA_SIZE;
	final int SPRITE_DATA_SIZE = 0x800;
	
	final int PIC_DATA_START = SPRITE_DATA_START + SPRITE_DATA_SIZE;
	final int PIC_ROW_DATA_SIZE = WIDTH/8;
	final int PIC_DATA_SIZE = PIC_ROW_DATA_SIZE*HEIGHT;
	
	final int PIC2_DATA_START = PIC_DATA_START + PIC_DATA_SIZE;

	final int BASE_FILE_SIZE = PIC_DATA_START + PIC_DATA_SIZE;
	
	byte[] mAtariFileData = null;
	
	private final int mPixelsPerByte = getPixelsPerByte();
	private final int mBitsPerPixel = 8/mPixelsPerByte;
	
	class HcmPalette {
		final int index;

		final HcmPalette paletteFrom;
		final HcmPalette paletteTo;
		
		final int[] atariColors;
		final int[] baseRgbColors;
		final boolean[] penalizeBaseRgb;
		final boolean[] isBaseRgbColorInterlaced;
		final int[] rgbColors;
		final int[][] hsvColors;
		final double[] brightness;
		
		final int ditherSteps;
		final int ditherArrayDimension;
		final int[][] ditherMap;
		
		final int[] baseColorFrom;
		final int[] baseColorTo;
		final double[] fromToDist;
		final double[] transitionProgress;
		final int[] ditherStep;
		
		int blackToDarkestFrom = -1;
		int blackToDarkestTo = -1;
		
		final int cnt;
		
		int ditherMapCnt = 0;
		
		public HcmPalette(int index, byte[] paletteFileData, int[] atariColors, boolean isInterlaced,
				int ditherMatrixDimensionPow, double respectBrightnessOnlyRatio, int[] penalizeColors) {
			this(index, paletteFileData, atariColors,isInterlaced,
					ditherMatrixDimensionPow, respectBrightnessOnlyRatio, penalizeColors, null, null);
		}
		
		public HcmPalette(int index, HcmPalette paletteFrom, HcmPalette paletteTo, boolean isInterlaced,
				int ditherMatrixDimensionPow, double respectBrightnessOnlyRatio) {
			this(index, null, null,isInterlaced,
					ditherMatrixDimensionPow, respectBrightnessOnlyRatio, null, paletteFrom, paletteTo);
		}
		
		private HcmPalette(int index, byte[] paletteFileData, int[] atariColors, boolean isInterlaced,
				int ditherMatrixDimensionPow, double respectBrightnessOnlyRatio, int[] penalizeColors,
				HcmPalette paletteFrom, HcmPalette paletteTo) {
			final boolean isTransitionPalette = paletteFrom!=null;
			final int baseCnt = isTransitionPalette?paletteFrom.baseRgbColors.length:
				(isInterlaced?(atariColors.length*2-1):atariColors.length);
			
			this.index = index;
			
			this.paletteFrom = paletteFrom;
			this.paletteTo = paletteTo;
			
			this.atariColors = atariColors;
			this.baseRgbColors = new int[baseCnt];
			this.isBaseRgbColorInterlaced = new boolean[baseCnt];
			this.penalizeBaseRgb = new boolean[baseCnt];
			
			this.ditherArrayDimension = (int) Math.pow(2, ditherMatrixDimensionPow);
			this.ditherSteps = ditherArrayDimension*ditherArrayDimension - 1; //steps without fromRgb/empty & toRgb/full
			this.ditherMap = new int[ditherArrayDimension][ditherArrayDimension];
			
			//Example for 5 baseCnt: 4+3+2+1= 10 connections
			//10 connections * ditherSteps + baseColorsRecordedOnce
			int cntTmp = baseCnt;
			for (int bC=0;bC<(baseCnt-1);bC++) {
				cntTmp+= (bC+1)*ditherSteps;
			}
			cnt = cntTmp;

			System.out.println(String.format("  Adding " + (isTransitionPalette?"transition ":"") + "palette: Working with %d combinations of %d base colors (%s dither steps)", cnt, baseCnt, ditherSteps));
			System.out.print("    Base colors: ");

			final List<Integer> penalizeList = new ArrayList<Integer>();
			if (penalizeColors!=null) {
				for (int cix : penalizeColors) {
					penalizeList.add(cix);
				}
			}
			
			if (!isTransitionPalette) {
				for (int bC=0;bC<baseCnt;bC++) {
					final boolean isColorInterlaced = isInterlaced && ((bC&1)==1);
					isBaseRgbColorInterlaced[bC] = isColorInterlaced;
	
					if (isInterlaced) {
						if (isColorInterlaced) {
							final int rgbLeft = atariToRgb(paletteFileData, atariColors[bC/2]);
							final int rgbRight = atariToRgb(paletteFileData, atariColors[bC/2+1]);
							baseRgbColors[bC] = rgbAverage(rgbLeft, rgbRight);
							penalizeBaseRgb[bC] = penalizeList.contains(atariColors[bC/2])||penalizeList.contains(atariColors[bC/2+1]);
						}
						else {
							baseRgbColors[bC] = atariToRgb(paletteFileData, atariColors[bC/2]);
							penalizeBaseRgb[bC] = penalizeList.contains(atariColors[bC/2]);
						}
					}
					else {
						baseRgbColors[bC] = atariToRgb(paletteFileData, atariColors[bC]);
						penalizeBaseRgb[bC] = penalizeList.contains(atariColors[bC]);
					}
					
					System.out.print("$" + Integer.toHexString(baseRgbColors[bC]) + " ");
				}
				System.out.println();
			}
			else {
				for (int bC=0;bC<baseCnt;bC++) {

					final int fromRgb = paletteFrom.baseRgbColors[bC];
					final int toRgb = paletteTo.baseRgbColors[bC];
					
					final int fromR = (fromRgb>>16)&0xff;
					final int fromG = (fromRgb>>8)&0xff;
					final int fromB = fromRgb&0xff;
					
					final int toR = (toRgb>>16)&0xff;
					final int toG = (toRgb>>8)&0xff;
					final int toB = toRgb&0xff;
	
					final double diffR = toR - fromR;
					final double diffG = toG - fromG;
					final double diffB = toB - fromB;
					
					final double progress = 0.5f;
					final int r = (int) ((double)fromR + (diffR*progress));
					final int g = (int) ((double)fromG + (diffG*progress));
					final int b = (int) ((double)fromB + (diffB*progress));
					final int rgb = r<<16|g<<8|b;
					
					baseRgbColors[bC] = rgb;
					penalizeBaseRgb[bC] = paletteFrom.penalizeBaseRgb[bC];
					System.out.print("$" + Integer.toHexString(baseRgbColors[bC]) + " ");
				}
			}
			
			rgbColors = new int[cnt];
			baseColorFrom = new int[cnt];
			baseColorTo = new int[cnt];
			fromToDist = new double[cnt];
			transitionProgress = new double[cnt];
			ditherStep = new int[cnt];
			
			int cix = 0;
			
			//Recording base colors first
			for (int bC=0;bC<baseCnt;bC++) {
				final int rgb = baseRgbColors[bC];

				//Record base color (from=to; progress=100%)
				baseColorFrom[cix] = bC;
				baseColorTo[cix] = bC;
				transitionProgress[cix] = 1f;
				ditherStep[cix] = 0;
				rgbColors[cix++] = rgb;
			}
			
			//Creating all connections/transitions between base colors
			for (int fromC=0;fromC<(baseCnt-1);fromC++) {
				for (int toC=fromC+1;toC<baseCnt;toC++) {
					final int fromRgb = baseRgbColors[fromC];
					final int toRgb = baseRgbColors[toC];
					
					final int fromR = (fromRgb>>16)&0xff;
					final int fromG = (fromRgb>>8)&0xff;
					final int fromB = fromRgb&0xff;
					
					final int toR = (toRgb>>16)&0xff;
					final int toG = (toRgb>>8)&0xff;
					final int toB = toRgb&0xff;
	
					final double diffR = toR - fromR;
					final double diffG = toG - fromG;
					final double diffB = toB - fromB;
					
					for (int d=0;d<ditherSteps;d++) {
						final double progress = 1f/(double)(ditherSteps+1)*(float)(d+1);
						final int r = (int) ((double)fromR + (diffR*progress));
						final int g = (int) ((double)fromG + (diffG*progress));
						final int b = (int) ((double)fromB + (diffB*progress));
						final int rgb = r<<16|g<<8|b;
	
						baseColorFrom[cix] = fromC;
						baseColorTo[cix] = toC;
						transitionProgress[cix] = progress;
						ditherStep[cix] = d;
						rgbColors[cix++] = rgb;
					}
					
//					System.out.println(String.format("Added transitions between: fromC=%d, toC=%d (cix=%d)", fromC, toC, cix));
				}
			}

//			System.out.println();
			
			this.hsvColors = new int[cnt][3];
			this.brightness = new double[cnt];
			for (int c=0;c<cnt;c++) {
				final int rgb = rgbColors[c];

				final int rByte = (rgb>>16)&0xff;
				final int gByte = (rgb>>8)&0xff;
				final int bByte = rgb&0xff;
				
				final float[] hsvFloat = Color.RGBtoHSB(rByte, gByte, bByte, null);
				hsvColors[c][0] = Math.round(255f * hsvFloat[0]);
				hsvColors[c][1] = Math.round(255f * hsvFloat[1]);
				hsvColors[c][2] = Math.round(255f * hsvFloat[2]);
				
				final double br = calculateBrightness(rByte, gByte, bByte);
				brightness[c] = br;
			}
			
			double blackToDarkestDist = Double.MAX_VALUE;
			
			for (int c=0;c<cnt;c++) {
				if (baseColorFrom[c]==baseColorTo[c]) {
					fromToDist[c] = 0f;
				}
				else {
					final int rgbFrom = baseRgbColors[baseColorFrom[c]];
					final int rgbTo = baseRgbColors[baseColorTo[c]];
					
					final int rByteFrom = (rgbFrom>>16)&0xff;
					final int gByteFrom = (rgbFrom>>8)&0xff;
					final int bByteFrom = rgbFrom&0xff;

					final int rByteTo = (rgbTo>>16)&0xff;
					final int gByteTo = (rgbTo>>8)&0xff;
					final int bByteTo = rgbTo&0xff;
					
					final float[] hsvFloatFrom = Color.RGBtoHSB(rByteFrom, gByteFrom, bByteFrom, null);
					final int fromH = Math.round(255f * hsvFloatFrom[0]);
					final int fromS = Math.round(255f * hsvFloatFrom[1]);
					final int fromV = Math.round(255f * hsvFloatFrom[2]);
					
					final float[] hsvFloatTo = Color.RGBtoHSB(rByteTo, gByteTo, bByteTo, null);
					final int toH = Math.round(255f * hsvFloatTo[0]);
					final int toS = Math.round(255f * hsvFloatTo[1]);
					final int toV = Math.round(255f * hsvFloatTo[2]);
					
					final double brightnessFrom = calculateBrightness(rByteFrom, gByteFrom, bByteFrom);
					final double brightnessTo = calculateBrightness(rByteTo, gByteTo, bByteTo);
					final double distBrightness = Math.abs(brightnessFrom-brightnessTo);
					final double distHsv = getHsvDistance(fromH, fromS, fromV, toH, toS, toV);
					fromToDist[c] = Math.sqrt(respectBrightnessOnlyRatio*distBrightness*distBrightness + (1f-respectBrightnessOnlyRatio)*distHsv*distHsv);
					
					if (brightnessFrom==0.0d && fromToDist[c]<blackToDarkestDist) {
						blackToDarkestDist = fromToDist[c];
						blackToDarkestFrom = baseColorFrom[c];
						blackToDarkestTo = baseColorTo[c];
					}
				}
			}
			
			generateDithermap();
		}

		private void generateDithermap() {
			for (int[] row : ditherMap) {
				Arrays.fill(row, -1);
			}
			
			ditherMapCnt = 0;

			final DitherMapSection ds = getDitherMapSection(0, 0, ditherArrayDimension);
			while (!ds.isDone()) {
				step(ds);
			}

		}

		private DitherMapSection getDitherMapSection(int x, int y, int dim) {
			DitherMapSection[] children = null;
			
			if (dim>=4) {
				final int childDim = dim/2;
				final DitherMapSection c1 = getDitherMapSection(x, y+childDim, childDim);
				final DitherMapSection c2 = getDitherMapSection(x+childDim, y, childDim);
				final DitherMapSection c3 = getDitherMapSection(x, y, childDim);
				final DitherMapSection c4 = getDitherMapSection(x+childDim, y+childDim, childDim);
				children = new DitherMapSection[]{c1, c2, c3, c4};
			}

			return new DitherMapSection(x, y, dim, children);
		}

		private void step(DitherMapSection ds) {
			if (!ds.isDone()) {
				if (ds.dim>2) {
					step(ds.children[ds.stepsDone]);
					ds.stepsDone = (ds.stepsDone+1)&3;
				}
				else {
					final int xInc = (ds.stepsDone==1 || ds.stepsDone==3)?1:0;
					final int yInc = (ds.stepsDone==0 || ds.stepsDone==3)?1:0;
					ditherMap[ds.x+xInc][ds.y+yInc] = ditherMapCnt++;
					ds.stepsDone++;
				}
			}
		}
		
		class DitherMapSection {
			final int x;
			final int y;
			final int dim;
			final DitherMapSection[] children;
			
			int stepsDone = 0;
			
			public DitherMapSection(int x, int y, int dim, DitherMapSection[] children) {
				this.x = x;
				this.y = y;
				this.dim = dim;
				this.children = children;
			}
			
			boolean isDone() {
				boolean result = false;
				
				if (dim>2) {
					int doneCnt = 0;
					for (DitherMapSection child : children) {
						if (child.isDone()) {
							doneCnt++;
						}
					}
					result = doneCnt==children.length;
				}
				else {
					result = stepsDone==4;
				}
				
				return result;
			}
		}
	}
	
	class AtariColorSpec implements Comparable {
		final int paletteSlot;
		final int colorIndex;
		final double brightness;
		final int rgb;
		
		public AtariColorSpec(int paletteSlot, int colorIndex, double brightness, int rgb) {
			this.paletteSlot = paletteSlot;
			this.colorIndex = colorIndex;
			this.brightness = brightness;
			this.rgb = rgb;
		}

		public String getHexColorIndex() {
			return (colorIndex<0x10?"0":"")+Integer.toHexString(colorIndex).toUpperCase();
		}
		
		@Override
		public int compareTo(Object o) {
			return Double.compare(brightness, ((AtariColorSpec) o).brightness);
		}
	}
	
	HcmPalette[] palettes = null;	
	MarvinAttributes 	attributes;
	private boolean[][]	arrMask;
	
	public void load(){
		attributes = getAttributes();
	}

	protected abstract int getPixelsPerByte();

	public void show(){
	}

	public void process(
		MarvinImage imageIn, 
		MarvinImage imageOut,
		MarvinAttributes attributesOut,
		MarvinImageMask mask, 
		boolean previewMode) {

		final boolean isReference = (Boolean)attributes.get("IS_REFERENCE");
		final int[] excludeColors = (int[]) attributes.get("excludeColors");

		final int w = isReference?640:WIDTH;
		final int h = isReference?480:HEIGHT;
		final boolean useMargin = (Boolean)attributes.get("um");
		final int mode = (Integer)attributes.get("m");
		final boolean mixPmPf = (Boolean)attributes.get("MIX_PM_PF");
		final boolean allowBlackDarkestDithering = (Boolean)attributes.get("ALLOW_BLACK_DARKEST_DITHERING");
		
		final double ignoreDitheringBelowBrightness = (Double) attributes.get("IGNORE_DITHERING_BELOW_BRIGHTNESS");
		final double ignoreDitheringAboveOrBelowDistPerc = (Double) attributes.get("IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE");
		final double respectBrightnessOnlyRatio = (Double) attributes.get("RESPECT_BRIGHTNESS_ONLY_RATIO");
		final double gamma = (Double) attributes.get("GAMMA");
		final int ditherMatrixDimensionPow = (Integer) attributes.get("DITHER_MATRIX_DIMENSION_POW");
		final double maxFromToDistanceToDither = (Double) attributes.get("MAX_FROM_TO_DISTANCE_TO_DITHER");
		final boolean underlayDithering = (Boolean)attributes.get("UNDERLAY_DITHERING");
		final double maxFromToDistanceToDitherUnderlay = (Double) attributes.get("MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY");

		final boolean isInterlaced = mode==MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_9_COLOR_INTERLACED;

		mAtariFileData = new byte[BASE_FILE_SIZE + (isInterlaced?PIC_DATA_SIZE:0)];
		
		final byte[] fingerPrint = HCM_FINGERPRINT.getBytes(StandardCharsets.US_ASCII);
		for (int f=0;f<fingerPrint.length;f++) {
			mAtariFileData[HCM_FINGERPRINT_DATA_START+f] = fingerPrint[f];
		}
		
		mAtariFileData[VERSION_DESCRIPTOR] = HCM_VERSION;
		mAtariFileData[MODE_DESCRIPTOR] = (byte) mode;
		
		try {
			initPalettes((String)attributes.get("p"), isInterlaced, mixPmPf, ditherMatrixDimensionPow,
					respectBrightnessOnlyRatio, excludeColors, underlayDithering,
					(Integer)attributes.get("COLOR_BCK"),
					(Integer)attributes.get("COLOR_UL1"),
					(Integer)attributes.get("COLOR_UL2"),
					(Integer)attributes.get("COLOR_PF0"),
					(Integer)attributes.get("COLOR_PF1"),
					(Integer)attributes.get("COLOR_PF2")
					);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		final int srcW = imageIn.getWidth();
		final int srcH = imageIn.getHeight();
		
		final float scaleFactor = getScaleFactor(w, h, srcW, srcH);
		
		final int newW = (int) (srcW * scaleFactor);
		final int newH = (int) (srcH * scaleFactor);
		
		final int paddingTop = (h - newH) / 2;
		final int paddingLeft = (w - newW) / 2;
		
		imageOut.resize(w, h);
		
		arrMask = mask.getMask();
			
		drawDownscaledImage(imageIn, imageOut, w, h, gamma, scaleFactor, paddingTop, paddingLeft);

		final ColorSpaceConverter colorspaceConverter = new ColorSpaceConverter();
		final MarvinImage hsvImage = new MarvinImage(w, h);
		colorspaceConverter.process(imageOut, hsvImage, attributesOut, mask, false);
		
		if (isReference) {
			final int bckgRgb = palettes[0].baseRgbColors[0];
			for (int y = 0; y < h; y++) {
				for (int x = 0; x < w; x++) {
					imageOut.setIntColor(x, y, (255<<24)+bckgRgb);
				}
			}

			final MarvinImagePlugin text = new Text();
			text.load();
			final MarvinImage font = MarvinImageIO.loadImage("res/font.png");
			text.setAttribute("fontFile", font);

			final Map<Integer, AtariColorSpec> colorMap = new HashMap<Integer, AtariColorSpec>();
			final int blockSize = 24;
			
			int pY = 0;
			
			final int paletteCnt = palettes.length;
			for (int p=0;p<paletteCnt;p++) {
				
				final HcmPalette palette = palettes[p];

				if (palette.paletteFrom==null) {
					final AtariColorSpec[] colors = new AtariColorSpec[palette.baseRgbColors.length];
					for (int c=0;c<palette.baseRgbColors.length;c++) {
						final int rgb = palette.baseRgbColors[c];
						final int r = (rgb>>16)&0xff;
						final int g = (rgb>>8)&0xff;
						final int b = rgb&0xff;
						final double brightness = calculateBrightness(r, g, b);
						final AtariColorSpec spec = new AtariColorSpec(c, palette.atariColors[isInterlaced?c/2:c], brightness, rgb);
						colors[c] = spec;
						
						colorMap.put(spec.colorIndex, spec);
					}
					
					Arrays.sort(colors);
					
					for (int c=0;c<colors.length;c++) {
						final AtariColorSpec spec = colors[c];
						final int x = 16 + c*blockSize;
						final int y = 16 + pY*blockSize;
	
						text(16+(colors.length+1)*blockSize+c*48, y, spec.getHexColorIndex(), imageOut, text, (255<<24)+spec.rgb);
						
						for (int yy=y;yy<y+blockSize;yy++) {
							for (int xx=x;xx<x+blockSize;xx++) {
								imageOut.setIntColor(xx, yy, (255<<24)+spec.rgb);
							}
						}
					}
					
					pY++;
				}
			}
			
			final Set<Integer> colorSet = colorMap.keySet();
			final AtariColorSpec[] allColors = new AtariColorSpec[colorSet.size()];
			int cx=0;
			for (Integer colorKey : colorSet) {
				allColors[cx] = colorMap.get(colorKey);
				cx++;
			}

			Arrays.sort(allColors);
			
			for (int c=0;c<allColors.length;c++) {
				final AtariColorSpec spec = allColors[c];
				final int y = 16 + (paletteCnt+1)*blockSize + c*blockSize;

				text(16+4*blockSize+16, y, spec.getHexColorIndex(), imageOut, text, (255<<24)+spec.rgb);

				for (int yy=y;yy<y+blockSize;yy++) {
					for (int xx=16;xx<4*blockSize+16;xx++) {
						imageOut.setIntColor(xx, yy, (255<<24)+spec.rgb);
					}
				}
			}
		}
		else
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x+=8) {
				final boolean isOnMargin = (x<COLOR_MARGIN_LEFT || x>=(w-COLOR_MARGIN_RIGHT));
				final int paletteCnt = isOnMargin?1:palettes.length;
				
				final double[][] paletteMinDistForPixel = new double[mPixelsPerByte][paletteCnt];
				final int[][] paletteMinDistForPixelColorIndex = new int[mPixelsPerByte][paletteCnt];
				
				for (int xOffs=0;xOffs<8;xOffs+=mBitsPerPixel) {
					//0..255 => 0..360
					final int iH = hsvImage.getIntComponent0(x+xOffs, y);
					//0..255 => 0..1f
					final int iS = hsvImage.getIntComponent1(x+xOffs, y);
					//0..255 => 0..1f
					final int iV = hsvImage.getIntComponent2(x+xOffs, y);
					
					for (int p=0;p<paletteCnt;p++) {
						final HcmPalette palette = palettes[p];
						
						paletteMinDistForPixel[xOffs/mBitsPerPixel][p] = Double.MAX_VALUE;
						paletteMinDistForPixelColorIndex[xOffs/mBitsPerPixel][p] = 0;
						
						calcMinDistancesForPalette(imageOut, allowBlackDarkestDithering, respectBrightnessOnlyRatio,
								maxFromToDistanceToDither, maxFromToDistanceToDitherUnderlay, y, x, paletteMinDistForPixel,
								paletteMinDistForPixelColorIndex, xOffs, iH, iS, iV, p, palette, false);
					}
				}
				
				double minPaletteMinDistSumForBlock = Double.MAX_VALUE;
				int winningPalette = 0;
				
				for (int p=0;p<paletteCnt;p++) {
					double paletteMinDistSumForBlock = 0;
					for (int px=0;px<mPixelsPerByte;px++) {
						paletteMinDistSumForBlock+= paletteMinDistForPixel[px][p];
					}
					
					if (paletteMinDistSumForBlock<minPaletteMinDistSumForBlock) {
						minPaletteMinDistSumForBlock = paletteMinDistSumForBlock;
						winningPalette = p;
					}
				}
				
				HcmPalette paletteFrom = palettes[winningPalette].paletteFrom;
				HcmPalette paletteTo = palettes[winningPalette].paletteTo;

				final boolean isTransitionPalette = paletteFrom!=null;
				if (isTransitionPalette) {
					winningPalette = (((x>>3)+y)&1)==0?paletteFrom.index:paletteTo.index;
				}
				
				final HcmPalette wp = palettes[winningPalette];

				for (int px=0;px<mPixelsPerByte;px++) {
					final int colorIndex = paletteMinDistForPixelColorIndex[px][winningPalette];
					final boolean isDithered = wp.baseColorFrom[colorIndex]!=wp.baseColorTo[colorIndex];
					int resultBaseColorIndex;
					
					if (useMargin || !isOnMargin) {
						if (isDithered) {
							final double tresholdSetting = wp.brightness[colorIndex]<ignoreDitheringBelowBrightness?
									1f:ignoreDitheringAboveOrBelowDistPerc;
							final double treshold = tresholdSetting * wp.fromToDist[colorIndex];
							final boolean isBelowTreshold = wp.transitionProgress[colorIndex]<treshold;
							final boolean isAboveTreshold = wp.transitionProgress[colorIndex]>(1f-treshold);
							if (!isBelowTreshold && !isAboveTreshold) {
								final int ditherStep = wp.ditherStep[colorIndex];
								
								final int ditherMapValue = 
										wp.ditherMap[(x + px)&(wp.ditherArrayDimension-1)][y&(wp.ditherArrayDimension-1)];
								final boolean doesJump = ditherStep>=ditherMapValue;
								resultBaseColorIndex = doesJump?wp.baseColorTo[colorIndex]:wp.baseColorFrom[colorIndex];
							}
							else {
								if (isBelowTreshold) {
									resultBaseColorIndex = wp.baseColorFrom[colorIndex];
								}
								else {
									resultBaseColorIndex = wp.baseColorTo[colorIndex];
								}
							}
						}
						else {
							resultBaseColorIndex = wp.baseColorFrom[colorIndex];
						}
					}
					else {
						//Winning palette is always #0 meaning:
						//No underlay, 0=background color
						resultBaseColorIndex = 0;
					}

					final int rgb = wp.baseRgbColors[resultBaseColorIndex];
					for (int subPx=0;subPx<mBitsPerPixel;subPx++) {
						imageOut.setIntColor(x + px*mBitsPerPixel+subPx, y, (255<<24)+rgb);
					}
					
					final int marginBytesLeft = COLOR_MARGIN_LEFT/8;
					final int marginBytesRight = COLOR_MARGIN_RIGHT/8;
					final int xByte = x/8;
					final int xByteColored = xByte-marginBytesLeft;

					final int byteOffset = xByte + y*PIC_ROW_DATA_SIZE;
					final int picDataAddr = PIC_DATA_START + byteOffset;
					final int pic2DataAddr = PIC2_DATA_START + byteOffset;
					
					if (isInterlaced) {
						final boolean isBaseRgbColorInterlaced =
								wp.isBaseRgbColorInterlaced[resultBaseColorIndex]
										/*&& resultBaseColorIndex>1*/;
						final int odd = isBaseRgbColorInterlaced?((x/mBitsPerPixel+px + y)&1):0;
						final int picPixel = (resultBaseColorIndex/2+odd);
						final int pic2Pixel = isBaseRgbColorInterlaced?
								(resultBaseColorIndex/2+1-odd):picPixel;
						final int picByte = picPixel<<(8-mBitsPerPixel-px*mBitsPerPixel);
						final int pic2Byte = pic2Pixel<<(8-mBitsPerPixel-px*mBitsPerPixel);
						mAtariFileData[picDataAddr]|= picByte;
						mAtariFileData[pic2DataAddr]|= pic2Byte;
					}
					else {
						mAtariFileData[picDataAddr]|= resultBaseColorIndex<<(8-mBitsPerPixel-px*mBitsPerPixel);
					}

					if (!isOnMargin && winningPalette>0) {
						int spriteStartOffset = 0;
						int spriteStartOffset2 = 0;
						int spritePixelIndex = 0;
						int spritePixelIndex2 = 0;

						if (mixPmPf) {
							if (xByteColored<8) {
								//Player0/2
								spriteStartOffset = winningPalette==1?0x400:0x600;
								spritePixelIndex = 7-xByteColored;
							}
							else if (xByteColored<16) {
								//Player1/3
								spriteStartOffset = winningPalette==1?0x500:0x700;
								spritePixelIndex = 7-(xByteColored-8);
							}
							else if (xByteColored<18) {
								//Missiles0/2
								spriteStartOffset = 0x300;
								spritePixelIndex = (1-(xByteColored-16))+(winningPalette==1?0:4);
							}
							else if (xByteColored<20) {
								//Missiles1/3
								spriteStartOffset = 0x300;
								spritePixelIndex = (1-(xByteColored-18))+(winningPalette==1?2:6);
							}
							else {
								//Player0/2 replicas
								spriteStartOffset = winningPalette==1?0:0x100;
								spritePixelIndex = 7-(xByteColored-20);
							}
	
							
							final int spriteByteAddr = SPRITE_DATA_START + spriteStartOffset + 0x20 + y;
							mAtariFileData[spriteByteAddr]|= 1<<spritePixelIndex;
						}
						else {
							if (xByteColored<8) {
								//Player0/1
								spritePixelIndex = 7-xByteColored;
								spritePixelIndex2 = spritePixelIndex;
								spriteStartOffset = 0x400;
								spriteStartOffset2 = 0x500;
							}
							else if (xByteColored<16) {
								//Player2/3
								spritePixelIndex = 7-(xByteColored-8);
								spritePixelIndex2 = spritePixelIndex;
								spriteStartOffset = 0x600;
								spriteStartOffset2 = 0x700;
							}
							else if (xByteColored<18) {
								//Missiles0/1
								spritePixelIndex = (1-(xByteColored-16))+0;
								spritePixelIndex2 = (1-(xByteColored-16))+2;
								spriteStartOffset = 0x300;
								spriteStartOffset2 = spriteStartOffset;
							}
							else if (xByteColored<20) {
								//Missiles2/3
								spritePixelIndex = (1-(xByteColored-18))+4;
								spritePixelIndex2 = (1-(xByteColored-18))+6;
								spriteStartOffset = 0x300;
								spriteStartOffset2 = spriteStartOffset;
							}
							else {
								//Player0/1 replicas
								spritePixelIndex = 7-(xByteColored-20);
								spritePixelIndex2 = spritePixelIndex;
								spriteStartOffset = 0;
								spriteStartOffset2 = 0x100;
							}

							if (winningPalette==1 || winningPalette==3) {
								final int spriteByteAddr = SPRITE_DATA_START + spriteStartOffset + 0x20 + y;
								mAtariFileData[spriteByteAddr]|= 1<<spritePixelIndex;
							}
							
							if (winningPalette==2 || winningPalette==3) {
								final int spriteByteAddr = SPRITE_DATA_START + spriteStartOffset2 + 0x20 + y;
								mAtariFileData[spriteByteAddr]|= 1<<spritePixelIndex2;
							}
						}
					}
				}
				
				
			}
		}
		
	}

	private void calcMinDistancesForPalette(MarvinImage imageOut, final boolean allowBlackDarkestDithering,
			final double respectBrightnessOnlyRatio,
			final double maxFromToDistanceToDither, final double maxFromToDistanceToDitherUnderlay, int y, int x,
			final double[][] paletteMinDistForPixel, final int[][] paletteMinDistForPixelColorIndex, int xOffs,
			final int iH, final int iS, final int iV, int p, final HcmPalette palette, final boolean baseColorOnly) {
		for (int c=0;c<palette.cnt;c++) {
			final boolean isDithered = palette.baseColorFrom[c]!=palette.baseColorTo[c];

			if (!isDithered || !baseColorOnly) {
				final int pH = palette.hsvColors[c][0];
				final int pS = palette.hsvColors[c][1];
				final int pV = palette.hsvColors[c][2];
				
				//distance for image pixel vs palette color
				final double distHsv = getHsvDistance(iH, iS, iV, pH, pS, pV);
				
				final int r = imageOut.getIntComponent0(x+xOffs, y);
				final int g = imageOut.getIntComponent1(x+xOffs, y);
				final int b = imageOut.getIntComponent2(x+xOffs, y);
				final double brightness = calculateBrightness(r, g, b);
				
				final double distBrightness = Math.abs(palette.brightness[c] - brightness);
				
				final boolean isBlackToDarkestException = palette.baseColorFrom[c]==palette.blackToDarkestFrom &&
						palette.baseColorTo[c]==palette.blackToDarkestTo && allowBlackDarkestDithering;
				
				final boolean shouldWin = isBlackToDarkestException||!isDithered||palette.fromToDist[c]<=maxFromToDistanceToDither;
				
				final double dist = Math.sqrt(respectBrightnessOnlyRatio*distBrightness*distBrightness
						+ (1f-respectBrightnessOnlyRatio)*distHsv*distHsv);
				
				if (shouldWin && dist<paletteMinDistForPixel[xOffs/mBitsPerPixel][p]
						&& !palette.penalizeBaseRgb[palette.baseColorFrom[c]]
								 && !palette.penalizeBaseRgb[palette.baseColorTo[c]]) {
					
					boolean canWin = palette.paletteFrom==null;
					
					if (!canWin) {
						final double distBrightnessP = Math.abs(palette.paletteFrom.brightness[c] - palette.paletteTo.brightness[c]);
						
						final int pFromH = palette.paletteFrom.hsvColors[c][0];
						final int pFromS = palette.paletteFrom.hsvColors[c][1];
						final int pFromV = palette.paletteFrom.hsvColors[c][2];

						final int pToH = palette.paletteTo.hsvColors[c][0];
						final int pToS = palette.paletteTo.hsvColors[c][1];
						final int pToV = palette.paletteTo.hsvColors[c][2];

						final double distHsvP = getHsvDistance(pFromH, pFromS, pFromV, pToH, pToS, pToV);
						
						final double fromToDistP = Math.sqrt(respectBrightnessOnlyRatio*distBrightnessP*distBrightnessP + (1f-respectBrightnessOnlyRatio)*distHsvP*distHsvP);
						
						final boolean isBlackToDarkestExceptionP = palette.baseColorFrom[c]==palette.blackToDarkestFrom &&
								palette.baseColorTo[c]==palette.blackToDarkestTo && allowBlackDarkestDithering;
						canWin = isBlackToDarkestExceptionP||(fromToDistP<=maxFromToDistanceToDitherUnderlay);
					}
					
					if (canWin) {
						paletteMinDistForPixel[xOffs/mBitsPerPixel][p] = dist;
						paletteMinDistForPixelColorIndex[xOffs/mBitsPerPixel][p] = c;
					}
				}
			}
		}
	}

	private void text(int x, int y, String s, MarvinImage imageOut, final MarvinImagePlugin text, int color) {
		text.setAttribute("x", x);
		text.setAttribute("y", y);
		text.setAttribute("text", s);
		text.setAttribute("color", color);
		text.process(imageOut, imageOut);
	}

	protected abstract void drawDownscaledImage(MarvinImage imageIn, MarvinImage imageOut, final int w, final int h,
			final double gamma, final float scaleFactor, final int paddingTop, final int paddingLeft);
	
	private void initPalettes(String path, boolean isInterlaced, boolean mixPmPf,
			int ditherMatrixDimensionPow, double respectBrightnessOnlyRatio,
			int[] excludeColors, boolean underlayDithering,
			final int colorBck,
			final int colorUnderlay1,
			final int colorUnderlay2,
			final int colorPlayfield0,
			final int colorPlayfield1,
			final int colorPlayfield2
			) throws IOException {
		final byte[] paletteFileData = new byte[0x300];
		FileInputStream f = null;
		try {
			f = new FileInputStream(path);
			int bytesRead = 0;
			int totalBytesRead = 0;
			while ((bytesRead=f.read(paletteFileData, totalBytesRead, paletteFileData.length-totalBytesRead))>0) {
				totalBytesRead+= bytesRead;
			}
			
			mAtariFileData[PALETTE_DATA_BCK] = (byte) colorBck;
			mAtariFileData[PALETTE_DATA_UNDERLAY1] = (byte) colorUnderlay1;
			mAtariFileData[PALETTE_DATA_UNDERLAY2] = (byte) colorUnderlay2;
			mAtariFileData[PALETTE_DATA_PLAYFIELD0] = (byte) colorPlayfield0;
			mAtariFileData[PALETTE_DATA_PLAYFIELD1] = (byte) colorPlayfield1;
			mAtariFileData[PALETTE_DATA_PLAYFIELD2] = (byte) colorPlayfield2;
			
			initModeSpecificPalettes(isInterlaced, mixPmPf, ditherMatrixDimensionPow, respectBrightnessOnlyRatio,
					colorBck, colorUnderlay1, colorUnderlay2,
					colorPlayfield0, colorPlayfield1, colorPlayfield2, paletteFileData,
					excludeColors);
			
			if (underlayDithering) {
				createPaletteTransitions(isInterlaced, mixPmPf, ditherMatrixDimensionPow, respectBrightnessOnlyRatio);
			}
			
		} finally {
			if (f!=null) {
				try {
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	private void createPaletteTransitions(boolean isInterlaced, boolean mixPmPf, int ditherMatrixDimensionPow, double respectBrightnessOnlyRatio) {
		final HcmPalette[] basePalettes = palettes;

		final int baseCnt = basePalettes.length;
		int cnt = baseCnt;
		for (int bC=0;bC<(baseCnt-1);bC++) {
			cnt+= bC+1;
		}

		palettes = new HcmPalette[cnt];

		int pix = 0;
		
		//Recording base palettes first
		for (int bP=0;bP<baseCnt;bP++) {
			palettes[pix++] = basePalettes[bP];
		}



		//Creating all connections/transitions between base palettes
		for (int fromP=0;fromP<(baseCnt-1);fromP++) {
			for (int toP=fromP+1;toP<baseCnt;toP++) {
				final HcmPalette palette =
						new HcmPalette(pix, basePalettes[fromP], basePalettes[toP], isInterlaced, ditherMatrixDimensionPow, respectBrightnessOnlyRatio);
				palettes[pix++] = palette;
			}
		}
	}

	protected abstract void initModeSpecificPalettes(boolean isInterlaced, boolean mixPmPf, int ditherMatrixDimensionPow,
			double respectBrightnessOnlyRatio, final int colorBck,
			final int colorUnderlay1, final int colorUnderlay2, final int colorPlayfield0, final int colorPlayfield1,
			final int colorPlayfield2, final byte[] paletteFileData,
			int[] excludeColors);

	protected int atariToRgb(byte[] paletteFileData, int c) {
		final int r = byteToUnsignedInt(paletteFileData[c*3]);
		final int g = byteToUnsignedInt(paletteFileData[c*3+1]);
		final int b = byteToUnsignedInt(paletteFileData[c*3+2]);
		return (r<<16)|(g<<8)|b;
	}

	protected int byteToUnsignedInt(byte b) {
		return b>=0?(int)b:((int)b)+0x100;
	}

	private double getHsvDistance(int h0, int s0, int v0, int h1, int s1, int v1) {
		final float hc0 = h0 / 255f * 360f;
		final float hc1 = h1 / 255f * 360f;
		
		final float dh = Math.min(Math.abs(hc1-hc0), 360-Math.abs(hc1-hc0)) / 180.0f;
		
		final float	ds = Math.abs(s1-s0) / 255f;
		final float dv = Math.abs(v1-v0) / 255f;
		
		final double result = Math.sqrt(dh*dh+ds*ds+dv*dv);
		
		return result;
	}
	
	private float getScaleFactor(float destW, float destH, float srcW, float srcH) {
	    float destAspect = destW / destH;
	    float srcAspect = srcW / srcH;

	    float scaleFactor;
	    if (destAspect < srcAspect) {
	        scaleFactor = destH / srcH;
	    }
	    else {
	        scaleFactor = destW / srcW;
	    }

	    return scaleFactor;
	}	
	protected int getPredominantRGB(MarvinImage a_image, int a_x, int a_y, int xUnits, int yUnits,
			double gamma){
		int l_red=-1;
		int l_green=-1;
		int l_blue=-1;
		
		for(int x=a_x; x<a_x+xUnits; x++){
			for(int y=a_y; y<a_y+yUnits; y++){
				if(x < a_image.getWidth() && y < a_image.getHeight()){
					if(arrMask != null && !arrMask[x][y]){
						continue;
					}
					
					if(l_red == -1)		l_red = a_image.getIntComponent0(x,y);
					else				l_red = (l_red+a_image.getIntComponent0(x,y))/2;
					if(l_green == -1)	l_green = a_image.getIntComponent1(x,y);
					else				l_green = (l_green+a_image.getIntComponent1(x,y))/2;
					if(l_blue == -1)	l_blue = a_image.getIntComponent2(x,y);
					else				l_blue = (l_blue+a_image.getIntComponent2(x,y))/2;	
				}				
			} 
		}
		return (gamma(l_red, gamma)<<16)+(gamma(l_green, gamma)<<8)+gamma(l_blue, gamma);
	}
	
	private int gamma(int c, double gamma) {
		return (int) (255 * (Math.pow((double) c / (double) 255, 1/gamma)));
	}

	@Override
	public MarvinAttributesPanel getAttributesPanel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected double calculateBrightness(final int rByte, final int gByte, final int bByte) {
		final double r = rByte/255f;
		final double g = gByte/255f;
		final double b = bByte/255f;
		
		final double br = Math.sqrt(0.299f * r*r + 0.587f * g*g + 0.114 * b*b);
		return br;
	}

	public int rgbAverage(int rgbLeft, int rgbRight) {
		final int r1 = (rgbLeft>>16)&0xff;
		final int g1 = (rgbLeft>>8)&0xff;
		final int b1 = rgbLeft&0xff;
		
		final int r2 = (rgbRight>>16)&0xff;
		final int g2 = (rgbRight>>8)&0xff;
		final int b2 = rgbRight&0xff;
		
		final int r = (int) (Math.round((double) r1 + (double) r2)/2f);
		final int g = (int) (Math.round((double) g1 + (double) g2)/2f);
		final int b = (int) (Math.round((double) b1 + (double) b2)/2f);
		
		return (r<<16)|(g<<8)|b;
	}

	public byte[] getAtariFileData() {
		return mAtariFileData;
	}

	public abstract int getMode(boolean isInterlaced, boolean mixPmPf);
	
}

