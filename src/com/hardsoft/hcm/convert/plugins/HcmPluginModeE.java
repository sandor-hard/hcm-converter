package com.hardsoft.hcm.convert.plugins;

import java.util.Arrays;

import marvin.image.MarvinImage;

public class HcmPluginModeE extends HcmPlugin {

	@Override
	protected void initModeSpecificPalettes(boolean isInterlaced, boolean mixPmPf, int ditherMatrixDimensionPow,
			double respectBrightnessOnlyRatio, final int colorBck,
			final int colorUnderlay1, final int colorUnderlay2, final int colorPlayfield0, final int colorPlayfield1,
			final int colorPlayfield2, final byte[] paletteFileData,
			int[] excludeColors) {
		
		if (mixPmPf) {
			//Playfield colors WILL mix with the underlay colors
			//Underlays WON'T mix with each other
			
			mAtariFileData[PALETTE_DATA_CMB1] = (byte) (colorUnderlay1|colorPlayfield0);
			mAtariFileData[PALETTE_DATA_CMB2] = (byte) (colorUnderlay1|colorPlayfield1);
			mAtariFileData[PALETTE_DATA_CMB3] = (byte) (colorUnderlay2|colorPlayfield2);
	
			final AtariColorSpec[] colors = new AtariColorSpec[PALETTE_DATA_SIZE];
			for (int c=0;c<colors.length;c++) {
				final int colorIndex = byteToUnsignedInt(mAtariFileData[PALETTE_DATA_START+c]);
				final int rgb = atariToRgb(paletteFileData, colorIndex);
				final int r = (rgb>>16)&0xff;
				final int g = (rgb>>8)&0xff;
				final int b = rgb&0xff;
				final double brightness = calculateBrightness(r, g, b);
				colors[c] = new AtariColorSpec(c, colorIndex, brightness, rgb);
			}
			
			Arrays.sort(colors);
			
			System.out.print("Brightness order: ");
			for (int c=0;c<colors.length;c++) {
				final AtariColorSpec spec = colors[c];
				final String hexColor = (spec.colorIndex<0x10?"0":"")+Integer.toHexString(spec.colorIndex);
				final String hexRgb = Integer.toHexString(spec.rgb);
				System.out.print(String.format("#%d $%s ($%s %f)  ", spec.paletteSlot, hexRgb, hexColor, spec.brightness));
			}
			
			System.out.println();
						
			palettes = new HcmPalette[]{
				new HcmPalette(0, paletteFileData, new int[]{
						colorBck,
						colorPlayfield0,
						colorPlayfield1,
						colorPlayfield2}, isInterlaced,
						ditherMatrixDimensionPow,
						respectBrightnessOnlyRatio,
						excludeColors),
				
				new HcmPalette(1, paletteFileData, new int[]{
						colorUnderlay1,
						colorPlayfield0|colorUnderlay1,
						colorPlayfield1|colorUnderlay1,
						colorUnderlay1}, isInterlaced,
						ditherMatrixDimensionPow,
						respectBrightnessOnlyRatio,
						excludeColors),
	
				new HcmPalette(2, paletteFileData, new int[]{
						colorUnderlay2,
						colorPlayfield0,
						colorPlayfield1,
						colorPlayfield2 | colorUnderlay2}, isInterlaced,
						ditherMatrixDimensionPow,
						respectBrightnessOnlyRatio,
						excludeColors)
			};
		}
		else {
			//Playfield colors will NOT mix with the underlay colors
			//Underlays WILL mix with each other

			palettes = new HcmPalette[]{
					new HcmPalette(0, paletteFileData, new int[]{
							colorBck,
							colorPlayfield0,
							colorPlayfield1,
							colorPlayfield2}, isInterlaced,
							ditherMatrixDimensionPow,
							respectBrightnessOnlyRatio,
							excludeColors),
					
					new HcmPalette(1, paletteFileData, new int[]{
							colorUnderlay1,
							colorPlayfield0,
							colorPlayfield1,
							colorPlayfield2}, isInterlaced,
							ditherMatrixDimensionPow,
							respectBrightnessOnlyRatio,
							excludeColors),
					
					new HcmPalette(2, paletteFileData, new int[]{
							colorUnderlay2,
							colorPlayfield0,
							colorPlayfield1,
							colorPlayfield2}, isInterlaced,
							ditherMatrixDimensionPow,
							respectBrightnessOnlyRatio,
							excludeColors),
					
					new HcmPalette(3, paletteFileData, new int[]{
							colorUnderlay1|colorUnderlay2,
							colorPlayfield0,
							colorPlayfield1,
							colorPlayfield2}, isInterlaced,
							ditherMatrixDimensionPow,
							respectBrightnessOnlyRatio,
							excludeColors)
			};
		}
	}

	@Override
	protected void drawDownscaledImage(MarvinImage imageIn, MarvinImage imageOut, final int w, final int h,
			final double gamma, final float scaleFactor, final int paddingTop, final int paddingLeft) {
		for (int y = 0; y < h; y++) {				
			for (int x = 0; x < w; x+=2) {
				final int yUnits = (int) (Math.ceil(1.0f/scaleFactor));
				final int l_rgb = getPredominantRGB(imageIn, (int) ((x-paddingLeft)/scaleFactor),
						(int) ((y-paddingTop)/scaleFactor), yUnits*2, yUnits, gamma);
				imageOut.setIntColor(x, y, (255<<24)+l_rgb);
				imageOut.setIntColor(x+1, y, (255<<24)+l_rgb);
			}
		}
	}

	@Override
	protected int getPixelsPerByte() {
		return 4;
	}

	@Override
	public int getMode(boolean isInterlaced, boolean mixPmPf) {
		return isInterlaced?
				(mixPmPf?MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_9_COLOR_INTERLACED:MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_7_COLOR_INTERLACED):
					(mixPmPf?MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_9_COLOR:MODE_DESCRIPTOR_VALUE_ANTIC_E_28_BYTES_7_COLOR);
	}

}
