package com.hardsoft.hcm.convert.plugins;

import marvin.image.MarvinImage;

public class HcmPluginModeF extends HcmPlugin {

	@Override
	protected void initModeSpecificPalettes(boolean isInterlaced, boolean mixPmPf, int ditherMatrixDimensionPow,
			double respectBrightnessOnlyRatio, final int colorBck,
			final int colorUnderlay1, final int colorUnderlay2, final int colorPlayfield0, final int colorPlayfield1,
			final int colorPlayfield2, final byte[] paletteFileData,
			int[] excludeColors) {
		
		palettes = new HcmPalette[]{
			new HcmPalette(0, paletteFileData, new int[]{
					colorPlayfield2,
					(colorPlayfield2&0xf0)|(colorPlayfield1&0x0f)}, isInterlaced,
					ditherMatrixDimensionPow,
					respectBrightnessOnlyRatio,
					excludeColors),
			
			new HcmPalette(1, paletteFileData, new int[]{
					colorUnderlay1,
					(colorUnderlay1&0xf0)|(colorPlayfield1&0x0f)}, isInterlaced,
					ditherMatrixDimensionPow,
					respectBrightnessOnlyRatio,
					excludeColors),
			
			new HcmPalette(2, paletteFileData, new int[]{
					colorUnderlay2,
					(colorUnderlay2&0xf0)|(colorPlayfield1&0x0f)}, isInterlaced,
					ditherMatrixDimensionPow,
					respectBrightnessOnlyRatio,
					excludeColors),
			
			new HcmPalette(3, paletteFileData, new int[]{
					colorUnderlay1|colorUnderlay2,
					((colorUnderlay1|colorUnderlay2)&0xf0)|(colorPlayfield1&0x0f)}, isInterlaced,
					ditherMatrixDimensionPow,
					respectBrightnessOnlyRatio,
					excludeColors)
		};
	}

	@Override
	protected void drawDownscaledImage(MarvinImage imageIn, MarvinImage imageOut, final int w, final int h,
			final double gamma, final float scaleFactor, final int paddingTop, final int paddingLeft) {
		for (int y = 0; y < h; y++) {				
			for (int x = 0; x < w; x++) {
				final int yUnits = (int) (Math.ceil(1.0f/scaleFactor));
				final int l_rgb = getPredominantRGB(imageIn, (int) ((x-paddingLeft)/scaleFactor),
						(int) ((y-paddingTop)/scaleFactor), yUnits, yUnits, gamma);
				imageOut.setIntColor(x, y, (255<<24)+l_rgb);
			}
		}
	}
	
	@Override
	protected int getPixelsPerByte() {
		return 8;
	}
	
	@Override
	public int getMode(boolean isInterlaced, boolean mixPmPf) {
		return isInterlaced?MODE_DESCRIPTOR_VALUE_ANTIC_F_28_BYTES_8_COLOR_INTERLACED:MODE_DESCRIPTOR_VALUE_ANTIC_F_28_BYTES_8_COLOR;
	}
	
}
