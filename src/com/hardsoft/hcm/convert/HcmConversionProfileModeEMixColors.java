package com.hardsoft.hcm.convert;

import com.hardsoft.hcm.convert.plugins.HcmPlugin;
import com.hardsoft.hcm.convert.plugins.HcmPluginModeE;

public class HcmConversionProfileModeEMixColors extends HcmConversionProfile {

	public HcmConversionProfileModeEMixColors() {

		IGNORE_DITHERING_BELOW_BRIGHTNESS = 0.25f;
		IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE = 0.0000001f;
		RESPECT_BRIGHTNESS_ONLY_RATIO = 0.99f;
		GAMMA = 1f;
		DITHER_MATRIX_DIMENSION_POW = 3;
		MAX_FROM_TO_DISTANCE_TO_DITHER = 0.25f;
		ALLOW_BLACK_DARKEST_DITHERING = true;
		UNDERLAY_DITHERING = true;
		MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY = 0.29f;

		MIX_PM_PF = true;
		COLOR_BCK = 0x00;
		COLOR_UL1 = 0x8a;
		COLOR_UL2 = 0xea;
		COLOR_PF0 = 0x06;
		COLOR_PF1 = 0x28;
		COLOR_PF2 = 0x86;
		
	}

	@Override
	HcmPlugin getPlugin() {
		return new HcmPluginModeE();
	}
	
}
