package com.hardsoft.hcm.convert;

import com.hardsoft.hcm.convert.plugins.HcmPlugin;
import com.hardsoft.hcm.convert.plugins.HcmPluginModeE;

public class HcmConversionProfileModeENonMixGrey extends HcmConversionProfile {

	public HcmConversionProfileModeENonMixGrey() {
		IGNORE_DITHERING_BELOW_BRIGHTNESS = 0.0f;
		IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE = 0.0000001f;
		RESPECT_BRIGHTNESS_ONLY_RATIO = 0.95f;
		GAMMA = 1f;
		DITHER_MATRIX_DIMENSION_POW = 1;
		MAX_FROM_TO_DISTANCE_TO_DITHER = 0.25f;
		ALLOW_BLACK_DARKEST_DITHERING = false;
		UNDERLAY_DITHERING = false;
		MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY = 0.0f;
		
		MIX_PM_PF = false;
		COLOR_BCK = 0x00;
		COLOR_UL1 = 0x04;
		COLOR_UL2 = 0x08;
		COLOR_PF0 = 0x06;
		COLOR_PF1 = 0x0a;
		COLOR_PF2 = 0x02;
	}

	@Override
	HcmPlugin getPlugin() {
		return new HcmPluginModeE();
	}
	
}
