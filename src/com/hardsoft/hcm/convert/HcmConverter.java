/*
    HARD Color Map converter
    to be used with HCM Atari XL/XE as introduced in the REHARDEN demo

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to improve and/or add new HCM modes
    (like interlace modes; char based modes using the VSCROL trick; modes mixed with GTIA modes; etc...)
 */

package com.hardsoft.hcm.convert;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.hardsoft.hcm.convert.plugins.HcmPlugin;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

public class HcmConverter  { 

	static HcmConversionProfile conversionProfile;
			
	static int[] excludeColors = null;
	
	final static String PALETTE_PATH = "palette/altirra.pal";
	
	public static void main(String[] args) {
		System.out.println("HCM - HARD Color Map converter v1.0");

		conversionProfile = new HcmConversionProfileModeEMixColors();
		
		createReferenceImage();
		convert("pics/tiger.png");
		convert("pics/parrot.jpg");
		
		conversionProfile = new HcmConversionProfileModeENonMixGrey();
		
		createReferenceImage();
		convert("pics/darthvader.jpg");
		convert("pics/orca.jpg");
		convert("pics/tut1.jpg");
		convert("pics/tut2.jpg");
		
	}

	private static void createReferenceImage() {
		convert(null);
	}
	
	private static void convert(final String path) {
		convert(path, false, false);
	}
	
	private static void convert(String path, final boolean isInterlaced, final boolean useMargin) {
		final boolean isReference = path==null;
		if (isReference) {
			path="pics/reference.jpg";
		}
		
		final MarvinImage image = MarvinImageIO.loadImage(path);

		System.out.println(String.format("Converting %s (isInterlaced=%b, useMargin=%b)", path, isInterlaced, useMargin));

		final HcmPlugin hcm = conversionProfile.getPlugin();
		hcm.load();
		hcm.setAttribute("um", useMargin);
		hcm.setAttribute("p", PALETTE_PATH);
		
		final int mode = hcm.getMode(isInterlaced, conversionProfile.MIX_PM_PF);
		
		hcm.setAttribute("m", mode);

		//Darker colors than this (0.5=50% brightness) won't get dithered
		//Set to 0.5f for Pseudo b&w interlaced; or 0.25 for colorful non-interlaced
		hcm.setAttribute("IGNORE_DITHERING_BELOW_BRIGHTNESS", Double.valueOf(conversionProfile.IGNORE_DITHERING_BELOW_BRIGHTNESS));

		//Dithering will be ignored below or above this treshold in distance between from and to colors
		hcm.setAttribute("IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE", Double.valueOf(conversionProfile.IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE));
		
		//1f = Pseudo-black&white; 0.99f-0.95f = colors+brightness
		hcm.setAttribute("RESPECT_BRIGHTNESS_ONLY_RATIO", Double.valueOf(conversionProfile.RESPECT_BRIGHTNESS_ONLY_RATIO));
		
		//1f==original image
		hcm.setAttribute("GAMMA", Double.valueOf(conversionProfile.GAMMA));
		
		hcm.setAttribute("DITHER_MATRIX_DIMENSION_POW", Integer.valueOf(conversionProfile.DITHER_MATRIX_DIMENSION_POW));

		hcm.setAttribute("COLOR_BCK", Integer.valueOf(conversionProfile.COLOR_BCK));
		hcm.setAttribute("COLOR_UL1", Integer.valueOf(conversionProfile.COLOR_UL1));
		hcm.setAttribute("COLOR_UL2", Integer.valueOf(conversionProfile.COLOR_UL2));
		hcm.setAttribute("COLOR_PF0", Integer.valueOf(conversionProfile.COLOR_PF0));
		hcm.setAttribute("COLOR_PF1", Integer.valueOf(conversionProfile.COLOR_PF1));
		hcm.setAttribute("COLOR_PF2", Integer.valueOf(conversionProfile.COLOR_PF2));
		
		hcm.setAttribute("MIX_PM_PF", conversionProfile.MIX_PM_PF);
		hcm.setAttribute("MAX_FROM_TO_DISTANCE_TO_DITHER", Double.valueOf(conversionProfile.MAX_FROM_TO_DISTANCE_TO_DITHER));
		hcm.setAttribute("ALLOW_BLACK_DARKEST_DITHERING", conversionProfile.ALLOW_BLACK_DARKEST_DITHERING);
		hcm.setAttribute("UNDERLAY_DITHERING", conversionProfile.UNDERLAY_DITHERING);
		hcm.setAttribute("MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY", Double.valueOf(conversionProfile.MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY));
		
		if (excludeColors!=null) {
			hcm.setAttribute("excludeColors", excludeColors);
		}
		
		hcm.setAttribute("IS_REFERENCE", isReference);

		hcm.process(image.clone(),image);
		image.update(); 
		MarvinImageIO.saveImage(image, String.format("%s.out.%d.bmp", path + (isInterlaced?"-I":""), System.currentTimeMillis()));
		
		if (!isReference) {
			final byte[] atariFileData = hcm.getAtariFileData();
			FileOutputStream f = null;
			try {
				f = new FileOutputStream(String.format("%s.out.%d.hcm", path + (isInterlaced?"-I":""), System.currentTimeMillis()));
				f.write(atariFileData);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (f!=null) {
					try {
						f.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		System.out.println(String.format("Finished converting %s (isInterlaced=%b, useMargin=%b)", path, isInterlaced, useMargin));
		System.out.println();
	}
}