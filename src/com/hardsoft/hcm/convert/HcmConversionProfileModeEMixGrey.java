package com.hardsoft.hcm.convert;

import com.hardsoft.hcm.convert.plugins.HcmPlugin;
import com.hardsoft.hcm.convert.plugins.HcmPluginModeE;

public class HcmConversionProfileModeEMixGrey extends HcmConversionProfile {

	public HcmConversionProfileModeEMixGrey() {
		IGNORE_DITHERING_BELOW_BRIGHTNESS = 0.25f;
		IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE = 0.0000001f;
		RESPECT_BRIGHTNESS_ONLY_RATIO = 0.99f;
		GAMMA = 1f;
		DITHER_MATRIX_DIMENSION_POW = 3;
		MAX_FROM_TO_DISTANCE_TO_DITHER = 0.25f;
		ALLOW_BLACK_DARKEST_DITHERING = true;
		UNDERLAY_DITHERING = true;
		MAX_FROM_TO_DISTANCE_TO_DITHER_UNDERLAY = 0.29f;
		
		MIX_PM_PF = true;
		COLOR_BCK = 0x00;
		COLOR_UL1 = 0x08;
		COLOR_UL2 = 0x02;
		COLOR_PF0 = 0x04;
		COLOR_PF1 = 0x06;
		COLOR_PF2 = 0x08;	
	}

	@Override
	HcmPlugin getPlugin() {
		return new HcmPluginModeE();
	}
	
}
